﻿using System;
using System.IO;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyGymPlan
{
    public partial class App : Application
    {
        static GymPlanDatabase database;
        public App()
        {

            InitializeComponent();

            MainPage = new MainPage();
        }

        public static GymPlanDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new GymPlanDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }
                return database;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
