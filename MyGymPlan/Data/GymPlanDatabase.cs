﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

using Xamarin.Forms;

namespace MyGymPlan
{
    public class GymPlanDatabase : ContentPage
    {
        readonly SQLiteAsyncConnection database;

        public GymPlanDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<DayItem>().Wait();
        }

        public Task<List<DayItem>> GetItemsAsync()
        {
            return database.Table<DayItem>().ToListAsync();
        }

        public Task<List<DayItem>> GetItemsNotDoneAsync()
        {
            return database.QueryAsync<DayItem>("SELECT * FROM [TodoItem] WHERE [Done] = 0");
        }

        public Task<DayItem> GetItemAsync(int id)
        {
            return database.Table<DayItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(DayItem item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(DayItem item)
        {
            return database.DeleteAsync(item);
        }
    }
}

